---
#See https://github.com/kevinoid/postgresql-for-wordpress for integrating wordpress with postgresql, as mysql is usually used
# Also see https://russellhaering.com/wordpress-with-ngingx-and-postgresql/ for how it can be done
- name: Installation of relevant software
  hosts: all
  become: true
  tasks:
    - name: Connection check
      ping:

    - name: Fix cache
      shell:
        cmd: apt-get update -y --allow-releaseinfo-change && apt-get -y upgrade
        warn: no
      changed_when: false

    - name: Add PHP PPA #Since Ubuntu repositories are quite behind
      apt_repository:
        repo: ppa:ondrej/php
        state: present

    - name: Fix cache
      shell:
        cmd: apt-get update -y --allow-releaseinfo-change
        warn: no
      changed_when: false

    - name: Install Python2.7 for modules and ssh
      apt:
        name: python2.7
        state: latest

    - name: Install unzip
      apt:
        name: unzip
        state: latest

    - name: Install latest PHP # Maybe php-* to install all plugins up here
      apt:
        pkg:
          - php-cli
          - php-fpm
          - php-xml
          - spawn-fcgi


    - name: Remove apache2 #Installed automatically with PHP
      apt:
        name: apache2
        state: absent

    - name: Install PostgreSQL (and maybe MySQL to get Wordpress to work)
      apt:
        pkg:
          - postgresql
          - postgresql-contrib
          - libpq-dev
          - python-psycopg2
          - python3-psycopg2
          - php-pgsql
          - mysql-server
          - php-mysql
          - python3-mysqldb



    - name: Generate new postgres user password #postgresql default generates empty root password
      command: openssl rand -hex 7 creates=/root/.my.cnf
      register: postgresql_password #register lets save as variable
     # If /root/.my.cnf doensn't exist, and the command is run
    - debug: msg="New Postgres password - {{ postgresql_password.stdout }}"
      when: postgresql_password.changed
      #If exists and command not run
    - debug: msg="Postgres password not changed"
      when: not postgresql_password.changed

    - name: Create wordpress database (can only work with user postgres)
      become: true
      become_user: postgres
      postgresql_db:
        name: wordpress

        # Consider assigning the password to a variable
    - name: Create wordpress user
      become: true
      become_user: postgres
      postgresql_user:
        db: wordpress
        name: wordpress
        password: bananas
        priv: "ALL"

    - name: Update postgres password #And output it to the screen
      become: true
      become_user: postgres
      postgresql_user:
        db: wordpress
        name: postgres
        #        login_host: "{{items}}"  #This option does not appear to work at all
        password: "{{postgresql_password.stdout}}"
        priv: "ALL" # Find a way to change the permissions for just the wordpress database
        #      with_items:
          #              - "{{ ansible_hostname }}"
          #              - 127.0.0.1 #three different ways to denote localhost
          #              - ::1
          #              - "localhost"
      when: postgresql_password.changed


    - name: Create my.cnf #Copies the username and password for the root PostgreSQL user, following template in provisioning/templates/postgresql/my.cnf file
      template:
        src: templates/postgresql/my.cnf
        dest: /root/.my.cnf #file will contain username and password for root PostgreSQL user
      when: postgresql_password.changed

    - name: Install nginx
      apt:
        name: nginx
        state: latest
        update_cache: true

    - name: Start nginx
      service:
        name: nginx
        state: started
        enabled: yes

    - name: Create nginx config
      template:
        src: templates/nginx/default
        dest: /etc/nginx/sites-available/default
      notify: restart nginx #trigger restart when config changed

      # Handlers can be placed anywhere, same indentation as tasks, BETTER WAY to deal with things that change.

    - name: Restart nginx
      service:
        name: nginx
        state: restarted


    # Wordpress stuff
    - name: Copy wordpress.zip into tmp
      copy:
        src: files/wordpress.zip
        dest: /tmp/wordpress.zip

    - name: Copy pg4wp plugin into tmp
      copy:
        src: files/postgresql-for-wordpress/pg4wp
        dest: /tmp

    - name: Copy db.php from pg4wp plugin separately for ease of copying
      copy:
        src: files/postgresql-for-wordpress/pg4wp/db.php
        dest: /tmp/db.php

    - name: Unzip wordpress
      unarchive:
        src: /tmp/wordpress.zip
        dest: /tmp
        copy: no # Copy tells Ansible that the file is already in the environment
        creates: /tmp/wordpress/wp-settings.php

    - name: Create project folder
      file:
        dest: /var/www/book.example.com
        state: directory

      # Replace all this cp shell commands with the copy module at some point
    - name: Copy Wordpress files
      command:
        cmd: cp -a /tmp/wordpress/. /var/www/book.example.com
        creates: /var/www/book.example.com/wp-settings.php

    - name: Copy pg4wp plugin
      command:
        cmd: cp -a /tmp/pg4wp /var/www/book.example.com/wp-content/plugins/
        creates: /var/www/book.example.com/wordpress/wp-content/db.php
      changed_when: false

    - name: Copy db.php from pg4wp plugin
      command:
        cmd: cp -a /tmp/db.php /var/www/book.example.com/wp-content/
        creates: /var/www/book.example.com/wordpress/wp-content/db.php
      changed_when: false

    - name: Create wp-config
      template:
        src: templates/wordpress/wp-config.php
        dest: /var/www/book.example.com/wp-config.php

        # Tries to select the first user for the wordpress database, will fail if database doesn't exist, triggers the restore
        # NEED TO FIX THIS! THE SQL statement is broken, in the normal, here and w/in the actual psql shell, appears to be empty or not exist
    - name: Does the database exist?
      shell:
        cmd: PGPASSWORD=bananas psql -U wordpress -d wordpress -h localhost -c 'SELECT ID FROM wordpress.wp_users LIMIT 1;'
      register: db_exist
      ignore_errors: true

      # If return code is greater than 0 (0 if successful), do this
    - name: Copy WordPresss DB
      copy:
        src: files/wp-database.sql
        dest: /tmp/wp-database.sql
      when: db_exist.rc > 0

      # Remember to check the wordpress creation password and consider assigning it to a variable
    - name: Import Wordpress DB
      postgresql_db:
        name: wordpress
        login_user: wordpress
        login_host: localhost
        login_password: bananas
        target: /tmp/wp-database.sql
        state: restore
      when: db_exist.rc > 0

    # Consider adding an automatic backup function using the  postgresql_db state: dump function
  handlers:
  - name: restart nginx
    service:
      name: nginx
      state: restarted
  - name: start spawn-fcgi service
    service:
      name: spawn-fcgi
      state: started
